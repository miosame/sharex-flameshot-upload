## prerequisites nginx/php on your server

You can just follow those instructions: https://web.archive.org/web/20190606205611/https://www.looklinux.com/how-to-fix-413-request-entity-too-large-error-in-nginx/

Short summary:

1. Your nginx config (located at `/usr/local/nginx/conf/nginx.conf`) needs to have (either in the http or server block): `client_max_body_size 100M;` - you can raise it to any value you want, e.g. `2G` would be a 2GB limit
2. Your `php.ini` (you can get the location for it via terminal: `php --ini`) should contain:

```
;The maximum size of an uploaded file.
upload_max_filesize = 100M

;Sets max size of post data allowed. This setting also affects file upload. To upload large files, this value must be larger than upload_max_filesize
post_max_size = 150M
```

Again: both values can be raised in the same fasion as the nginx config.

## prerequisites only for flameshot (linux)

1. Install:
    1. xclip (for copying link to clipboard after upload)
    2. dunst (for notifications)

## prerequisites for both sharex and flameshot

1. Upload `index.php` to your host
2. Replace `[password]`
3. Optional: replace the folder it should upload to, default is "i" in the documentroot folder

## sharex

Right click the sharex trayicon and click "Custom uploader settings":

![sharex contextmenu](https://p.miosa.me/miosame/sharex-flameshot-upload/raw/branch/master/screenshots/contextmenu.png)

Now change the settings as described below:

![sharex settings](https://p.miosa.me/miosame/sharex-flameshot-upload/raw/branch/master/screenshots/sharex_custom_settings.PNG)


1. Replace the URL with the full public path to your `index.php`
2. Set the `user` post variable to your `index.php` password
3. Set the `domain` post variable to the public domain (excl. subfolder, just the domain)

Example:
1. URL: `https://example.com`
2. domain: `example.com`

That would upload to `https://example.com/index.php` and return a url with `https://example.com/i/[md5].png` - given you're using the `i` upload folder default.

## flameshot
1. Make upload.sh executable: `chmod +x upload.sh`
2. Replace `[password]` with your `index.php` password
2. Execute flameshot via `flameshot gui -r | ./upload.sh example.com` where `example.com` is your public path to the `index.php`, so if you have it in a subfolder it should be `example.com/subfolder`
