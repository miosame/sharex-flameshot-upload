#!/usr/bin/env bash
output=$(curl -F "user=[password] -F "form=@-;filename=file.png" -F "domain=$1" -o - https://$1)
echo $output | xargs echo -n | xclip -selection clipboard
notify-send "Screenshot uploaded" "$output"